
var Pomodoro = function() {

	function init() {
		window.audioPomodoro = document.createElement("audio");
		window.audioPomodoro.id = "pomodoro_tictac";
		window.audioPomodoro.preload = "auto";
		window.audioPomodoro.loop = true;
		window.audioPomodoro.src = 'assets/audio/tictac.mp3';
		document.body.appendChild(window.audioPomodoro);
		window.audioPomodoro.play();
	}

	function startCowntDown(duration, display) {
		if(window.remainingTime) {
			duration = window.remainingTime;
		}

		if(!window.playingPomodoro) {
			
			var timer = duration, minutes, seconds;
			window.startTime = new Date();

			window.playingPomodoro = true;

		    window.cowntDownPomodoro = setInterval(function () {
		        minutes = parseInt(timer / 60, 10);
		        seconds = parseInt(timer % 60, 10);

		        minutes = minutes < 10 ? "0" + minutes : minutes;
		        seconds = seconds < 10 ? "0" + seconds : seconds;

		        display.text(minutes + ":" + seconds);

		        if (--timer < 0) {
		            timer = duration;

		            window.audioPomodoro.pause();
		            window.audioPomodoro.src = 'assets/audio/alarm.mp3';
		            window.audioPomodoro.loop = false;
		            stopCowntDown(duration, display);
		            window.audioPomodoro.play();

		            $('.pomodoro_start').text($('.pomodoro_start').data('start-label'));
		        } else {
			        window.remainingTime = timer;
			        localStorage.setItem('pomodoro_remaining',remainingTime);
		        }

		    }, 1000);

		}
	}

	function stopCowntDown(duration, display) {
		clearTimeout(window.cowntDownPomodoro);
		window.playingPomodoro = false;
		window.remainingTime = undefined;
		window.audioPomodoro.pause();
		localStorage.setItem('pomodoro_remaining', null);

		var timer = duration, minutes, seconds;
		minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);   
	}

	function pauseCowntDown() {
	    window.clearTimeout(window.cowntDownPomodoro);
	    window.playingPomodoro = false;
	    window.audioPomodoro.pause();
	    localStorage.setItem('pomodoro_remaining',null);
	}

	return {
		init: function() {
			init();
		},
		start: function(duration, display) {
			init();
			startCowntDown(duration, display);
		},
		pause: function() {
			pauseCowntDown();
		},
		stop: function(duration, display) {
			stopCowntDown(duration, display);
		},
	}
};

$(document).ready(function() {
	var pomodoro = new Pomodoro();
	var pomodoroStartBtn = $('.pomodoro_start');
	var pomodoroStopBtn = $('.pomodoro_stop');

	var displayCountdown = $(pomodoroStartBtn.data('target'));

	if(localStorage.getItem('pomodoro_remaining') != "null") {
		var timeCountdown = localStorage.getItem('pomodoro_remaining');
		pomodoro.start(timeCountdown, displayCountdown);

		pomodoroStartBtn.addClass('playing');
		pomodoroStartBtn.text(pomodoroStartBtn.data('pause-label'));
	} else {
		var timeCountdown = 60 * pomodoroStartBtn.data('countdown');
		localStorage.setItem('pomodoro_remaining',null);
	}

	pomodoroStartBtn.click(function(e){
		e.preventDefault();

	    //If is not playing => play
		if(!window.playingPomodoro) {
			pomodoro.start(timeCountdown, displayCountdown);
			pomodoroStartBtn.addClass('playing');
			pomodoroStartBtn.text(pomodoroStartBtn.data('pause-label'));
		}else{ //else => pause
			pomodoro.pause();
			pomodoroStartBtn.removeClass('playing');
			pomodoroStartBtn.text(pomodoroStartBtn.data('start-label'));
		}
	});

	pomodoroStopBtn.click(function(e){
		e.preventDefault();
	    var pomodoro = new Pomodoro();
		pomodoro.stop(timeCountdown, displayCountdown);
		
		pomodoroStartBtn.removeClass('playing');
		pomodoroStartBtn.text(pomodoroStartBtn.data('start-label'));
	});

});