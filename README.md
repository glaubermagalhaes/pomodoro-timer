# Tutorial #

This is a pomodoro timer made in JavaScript.

### If you need a pomodoro timer with "tictac" sound and an alarm at its end, please feel free to contribute. ###

* It uses audio element, so it is only compatible with browsers that support HTML5

### Set up ###

* Import jquery (1.11.3 or higher)
* Import pomodoro.js
* Add .pomodoro_start class to element that starts the counter and:
* - data-target attribute to set the element that will receive the result of counter
* - data-countdown attribute to set the start time
* - data-start-label to set the label when the counter is pause or stoped
* - data-pause-label to set the label when the counter is playing
* Add .pomodoro_stop class to element that stops the counter